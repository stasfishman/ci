import os

try:
    with open('data_files/response.txt', 'r') as f:
        l = f.readlines()
        print(l)
        assert '200' in l[0]
except Exception as e:
    print(e)
finally:
    os.remove('data_files/response.txt')